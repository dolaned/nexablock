import { createPinia } from 'pinia'

const pinia = createPinia()
pinia.use(({ store }) => {
    if (store.$id === 'theme') {
        store.initializeTheme()
    }
});

export default defineNuxtPlugin((nuxtApp) => {
    nuxtApp.vueApp.use(pinia)
})
