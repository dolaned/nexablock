/** @type {import('tailwindcss').Config} */
import typography from "@tailwindcss/typography";
export default {
  content: [
    './components/**/*.{vue,js}',
    './layouts/**/*.vue',
    './pages/**/*.vue',
    './plugins/**/*.{js,ts}',
    './nuxt.config.{js,ts}',
    './app.vue'
  ],
  safelist: [
    'blue-3xl',
    'rounded-full'
  ],
  theme: {
    extend: {
      colors: {
        primary: '#135e89',    // Dark blue for primary elements
        secondary: '#ffe144',  // Bright yellow for secondary elements
        accent: '#d0e2e8',     // Light blue for accents
        neutral: '#0F172A',    // Dark for neutral tones
        'base-100': '#000',    // Dark blue almost black background
        'base-200': '#223761', // Darker shade for secondary backgrounds
        'base-300': '#314f8c', // Dark blue background for tertiary surfaces
        info: '#2094f3',       // Information color
        success: '#009485',    // Success color
        warning: '#ff9900',    // Warning color
        error: '#ff5724',      // Error color
        'base-content': '#FFFFFF', // White for base body text color in dark mode
        'dark-color-start': 'hsl(231, 45%, 9%)',
        'dark-color-black': 'hsla(201, 100%, 1%, 1)',
      },
      backgroundImage: {
        'dark-gradient': 'linear-gradient(to bottom, var(--tw-gradient-stops))',
      },
      borderRadius: {
        'box': '0.25rem',  // Matching --rounded-box
        'btn': '0.25rem',  // Matching --rounded-btn
      },
      fontFamily: {
        'sans': ['"Open Sans"', 'sans-serif'],
        'roboto': ['Roboto', 'sans-serif'],
      },
      blur: {
        '3xl': '64px',
        '4xl': '80px',
      },
      spacing: {
        '120': '30rem',
        '1.25': '0.3125rem'
      },
    }
  },
  variants: {
    extend: {
      backgroundImage: ['dark'], // Enable dark mode for background images
    },
  },
  plugins: [typography],
};
